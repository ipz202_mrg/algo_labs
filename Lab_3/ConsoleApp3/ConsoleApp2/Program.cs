﻿using System;
using System.Text;
using System.Diagnostics;
namespace lab3
{
    class Program
    {

        static uint func(int n)
        {

            uint[] mass = new uint[n + 1]; mass[0] = 0; mass[1] = 1;

            uint result = 0;
            for (int i = 2; i < n; i++)
            {
                result = 0;
                mass[i] = mass[i - 1] + mass[i - 2];
                result = mass[i];
            }
            return result;
        }
        static void Main(string[] args)
        {

            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;
            int n;
            Stopwatch stopWatch = new Stopwatch();
            Console.WriteLine("Введіть номер числа з послідовності <90"); n = int.Parse(Console.ReadLine());
            stopWatch.Start();
            Console.WriteLine($"Результат:{func(n)}");
            stopWatch.Stop();
            TimeSpan ts = stopWatch.Elapsed;
            string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}",
                ts.Hours, ts.Minutes, ts.Seconds,
                ts.Milliseconds / 10);
            Console.WriteLine("--------------");
            Console.WriteLine("Час виконання: " + elapsedTime);

        }
    }
}
