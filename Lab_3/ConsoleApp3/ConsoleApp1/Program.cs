﻿using System;
using System.Text;

namespace lab3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;
            int n, menu;
            Console.WriteLine("Оберіть функцію:\n1) f(n)=n\n2) f(n)=log(n)\n3) f(n)=n*log(n)\n4) f(n)=n^2\n5) f(n)=2^n\n6) f(n)=n!\n0) Вихід");

            double x, y, z, r, q, s;
            do
            {
                menu = Convert.ToInt32(Console.ReadLine());
                switch (menu)
                {
                    case 1:
                        for (n = 0; n <= 50; n++)
                        {
                            x = n;
                            Console.WriteLine("n = {0}, f(n) = {1}", n, (int)x);
                        }
                        break;
                    case 2:
                        for (n = 1; n <= 50; n++)
                        {
                            y = Math.Log(n);
                            Console.WriteLine("n = {0}, f(n) = {1:F2}", n, y);
                        }
                        break;
                    case 3:
                        for (n = 1; n <= 50; n++)
                        {
                            z = n * Math.Log(n);
                            Console.WriteLine("n = {0}, f(n) = {1:F2}", n, z);
                        }
                        break;
                    case 4:
                        for (n = 0; n <= 50; n++)
                        {

                            r = Math.Pow(n, 2);
                            Console.WriteLine("n = {0}, f(n) = {1}", n, (int)r);
                            if (r > 500) break;
                        }
                        break;
                    case 5:
                        for (n = 0; n <= 50; n++)
                        {
                            q = Math.Pow(2, n);
                            Console.WriteLine("n = {0}, f(n) = {1}", n, (int)q);
                            if (q > 500) break;
                        }
                        break;
                    case 6:
                        for (n = 0; n <= 50; n++)
                        {
                            int factorial = 1;
                            for (int i = 1; i <= n; i++)
                            {
                                factorial *= i;
                            }
                            s = factorial;
                            if (s > 500) break;
                            Console.WriteLine("n = {0}, f(n) = {1}", n, (int)s);
                        }
                        break;
                }
            } while (menu != 0);
        }
    }
}
