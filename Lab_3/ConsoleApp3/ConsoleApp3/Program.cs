﻿using System;
using System.Text;
using System.Diagnostics;

namespace lab3_2._2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;

            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();

            Random rnd = new Random();
            Console.WriteLine("Генерація масиву:");
            Console.WriteLine("--------------");
            int[] array = new int[rnd.Next(1, 20)];
            string num = "";
            for (int i = 0; i < array.Length; i++)
            {
                array[i] = rnd.Next(0, 9);
                Console.WriteLine(array[i]);
            }
            Array.Sort(array);
            Array.Reverse(array);
            for (int i = 0; i < array.Length; i++)
            {
                num += array[i];
            }
            Console.WriteLine("--------------");
            Console.WriteLine($"Max: {num}");
            stopWatch.Stop();
            TimeSpan ts = stopWatch.Elapsed;

            string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}",
                ts.Hours, ts.Minutes, ts.Seconds,
                ts.Milliseconds / 10);
            Console.WriteLine("--------------");
            Console.WriteLine("Час виконання: " + elapsedTime);

        }
    }
}
