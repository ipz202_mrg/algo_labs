﻿using System;
using System.Text;

namespace lab2
{
    class Program
    {
        public static void Random(ulong x, int[] arrRand, int a = 22695477, int c = 1, int k = 0)
        {
            ulong m = (ulong)Math.Pow(2, 32);

            if (k < 8000)
            {
                x = ((ulong)a * x + (ulong)c) % m;
                k++;
                Random(x, arrRand, a, c, k);
                arrRand[k - 1] = (int)(x % 250);
            }
            else
            {
                return;
            }
        }

        public static int Frequency(int[] arr, int num)
        {
            int freq = 0;
            for (int i = 0; i < 8000; i++)

            {
                if (arr[i] == num)
                {
                    freq++;
                }
            }
            return freq;
        }
        public static double MathExpectation(int[] arr, int[] frequency)
        {
            double mathExp = 0;
            for (int i = 0; i < 250; i++)
            {
                mathExp += (arr[i] * frequency[i]) / 8000;
            }
            return mathExp;
        }

        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.Unicode;
            ulong x = 0;
            int[] mas = new int[8000];
            Random(x, mas);
            Array.Reverse(mas);
            int[] frequency = new int[8000];

            for (int i = 0; i < mas.Length; i++)
            {
                frequency[i] = Frequency(mas, mas[i]);
                Console.WriteLine($"№{i + 1}  {mas[i]}  К-сть повторів: {frequency[i]}  Статична ймовірність: {(double)frequency[i] / 8000}");
            }

            Console.WriteLine($"\nМатематичне сподівання випадкових величин:{MathExpectation(mas, frequency)}");

            double disp = 0;
            for (int i = 0; i <= 250; i++)
            {
                disp += Math.Pow((mas[i] - MathExpectation(mas, frequency)), 2) * frequency[i] / 8000;
            }
            Console.WriteLine($"Дисперсія випадкових величин:{disp}");

            double deviation = 0;
            for (int i = 0; i <= 250; i++)
            {
                deviation = Math.Sqrt(disp);
            }
            Console.WriteLine($"Середньоквадратичне відхилення випадкових величин:{deviation}");
        }

    }
}

