﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ReversePolishCounter
{
    class RPN
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.Unicode;
            while (true)
            {
                try
                {
                    Console.Write("Введіть вираз: ");
                    Console.WriteLine(Counting(Console.ReadLine()));
                }
                catch
                {
                    Console.WriteLine("Помилка. Перевірте правильність введення даних");
                }
            }
        }

        static public double Calculate(string input)//"вхідний" метод класу
        {

            string output = GetExpression(input);//запис виразу у постфіксній формі
            double result = Counting(output);
            return result;

        }



        static public string GetExpression(string input)//метод переведення виразу у постфіксну форму
        {
            string output = string.Empty;
            Stack<char> operStack = new Stack<char>();

            for (int i = 0; i < input.Length; i++)
            {
                //використання пробілу як запису зворотнього польського запису
                if (IsDelimeter(input[i]))
                {
                    continue;
                }

                //перевірка на від'ємне число
                if (input[i] == '-' && ((i > 0 && !Char.IsDigit(input[i - 1])) || i == 0))
                {
                    i++;
                    output += "-";
                }

                //Якщо символ цифра - зчитуємо все число
                if (Char.IsDigit(input[i]))
                {
                    while (!IsDelimeter(input[i]) && !IsOperator(input[i]))
                    {
                        output += input[i];
                        i++;

                        if (i == input.Length)
                        {
                            break;
                        }
                    }
                    output += " ";
                    i--;
                }

                //Якщо символ - знак
                if (IsOperator(input[i]))
                {

                    if (input[i] == '(')
                    {
                        operStack.Push(input[i]);
                    }
                    else if (input[i] == ')')
                    {
                        //Випусуємо всі оператори до "(" в рядок
                        char s = operStack.Pop();


                        while (s != '(')
                        {
                            output += s.ToString() + ' ';

                            s = operStack.Pop();
                        }
                    }
                    else
                    {
                        if (operStack.Count > 0)
                            if (GetPriority(input[i]) <= GetPriority(operStack.Peek())) //перевірка на пріоритетність
                            {
                                output += operStack.Pop().ToString() + " ";
                            }
                        operStack.Push(char.Parse(input[i].ToString()));

                    }
                }
            }

            //Після зчитування усіх символів, викидаємо зі стеку всі оператори, що там залишилися
            while (operStack.Count > 0)
            {
                output += operStack.Pop() + " ";
            }
            return output;
        }

        static private double Counting(string output)//метод підрахунку зворотнього польського запису
        {
            string result;
            string[] mas = output.Split(' ');

            for (int i = 0; i < mas.Length; i++)

                switch (mas[i])
                {
                    case "+":
                        result = (double.Parse(mas[i - 2]) + double.Parse(mas[i - 1])).ToString();//виконання операції
                        mas[i - 2] = result;//на місце першого операнда записуємо результат
                        for (int j = i - 1; j < mas.Length - 2; j++)//видаляємо другий операнд і знак
                            mas[j] = mas[j + 2];
                        Array.Resize(ref mas, mas.Length - 2);//обрізаємо масив на 2 видалених елемента
                        i -= 2;
                        break;


                    case "-":
                        result = (double.Parse(mas[i - 2]) - double.Parse(mas[i - 1])).ToString();
                        mas[i - 2] = result;
                        for (int j = i - 1; j < mas.Length - 2; j++)
                            mas[j] = mas[j + 2];
                        Array.Resize(ref mas, mas.Length - 2);
                        i -= 2;
                        break;

                    case "*":
                        result = (double.Parse(mas[i - 2]) * double.Parse(mas[i - 1])).ToString();
                        mas[i - 2] = result;
                        for (int j = i - 1; j < mas.Length - 2; j++)
                            mas[j] = mas[j + 2];
                        Array.Resize(ref mas, mas.Length - 2);
                        i -= 2;
                        break;

                    case "/":
                        result = (double.Parse(mas[i - 2]) / double.Parse(mas[i - 1])).ToString();
                        mas[i - 2] = result;
                        for (int j = i - 1; j < mas.Length - 2; j++)
                            mas[j] = mas[j + 2];
                        Array.Resize(ref mas, mas.Length - 2);
                        i -= 2;
                        break;


                    case "^":
                        result = (Math.Pow(double.Parse(mas[i - 2]), double.Parse(mas[i - 1]))).ToString();
                        mas[i - 2] = result;
                        for (int j = i - 1; j < mas.Length - 2; j++)
                            mas[j] = mas[j + 2];
                        Array.Resize(ref mas, mas.Length - 2);
                        i -= 2;
                        break;
                }
            return double.Parse(mas[0]);
        }

        //Метод, що повертає пріорітетність оператора
        static private byte GetPriority(char s)
        {
            switch (s)
            {
                case '(': return 0;
                case ')': return 1;
                case '+': return 2;
                case '-': return 3;
                case '*': return 4;
                case '/': return 4;
                case '^': return 5;
                default: return 6;
            }
        }
        //Перевірки на оператор та на пробіл
        static private bool IsOperator(char с)
        {
            if (("+-/*^()".IndexOf(с) != -1))
                return true;
            return false;
        }

        static private bool IsDelimeter(char c)
        {
            if ((" =".IndexOf(c) != -1))
                return true;
            return false;
        }
    }
}
