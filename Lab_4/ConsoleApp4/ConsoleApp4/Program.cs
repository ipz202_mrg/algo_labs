﻿
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Console;

namespace Algo
{
    public class doubleNode<T>
    {
        public T Data;
        public doubleNode<T> Previous;
        public doubleNode<T> Next;

        public doubleNode(T Data)
        {
            this.Data = Data;
        }
    }







    public class DoubleLinkedList<T>
    {
        doubleNode<T> head;
        doubleNode<T> tail;
        public int count { get; private set; }

        public void Add(T data)
        {
            doubleNode<T> node = new doubleNode<T>(data);

            if (head == null)
                head = node;

            else
            {
                tail.Next = node;
                node.Previous = tail;
            }
            tail = node;
            count++;
        }

        public void Firstly(T data)
        {
            doubleNode<T> node = new doubleNode<T>(data);
            doubleNode<T> temp = head;
            node.Next = temp;
            head = node;

            if (count == 0)
                tail = head;
            else
                temp.Previous = node;

            count++;
        }
        public void Show()
        {
            doubleNode<T> current = head;

            for (int i = 1; i <= count; i++)
            {
                WriteLine($"{i}. {current.Data}");
                current = current.Next;
            }
        }

        public bool Remove(T data)
        {
            doubleNode<T> List = head;

            while (List != null)
            {
                if (List.Data.Equals(data))
                    break;

                List = List.Next;
            }

            if (List != null)
            {
                if (List.Next != null)
                    List.Next.Previous = List.Previous;

                else
                    tail = List.Previous;

                if (List.Previous != null)
                    List.Previous.Next = List.Next;

                else
                    head = List.Next;

                count--;
                return true;
            }
            return false;
        }

        public void Vini()
        {
            head = null;
            tail = null;
            count = 0;
        }

        
    }

    class Program
    {
        static void Main(string[] args)
        {
            DoubleLinkedList<int> a = new DoubleLinkedList<int>();

            a.Add(4);
            a.Add(6);
            a.Add(11);
            a.Add(9);
            a.Add(18);
            a.Firstly(96);
            a.Add(18);
            a.Add(122);

            a.Remove(8);
            a.Show();

            a.Vini();
            a.Show();
        }
    }
}
